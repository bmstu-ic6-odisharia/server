CROSS_COMPILE=armv7-bbb-linux-gnueabihf-
CFLAGS=-Wall
#-I/usr/local/x-tools/beaglebone-black/lib/gcc/armv7-bbb-linux-gnueabihf/5.2.0/include/ -I/usr/local/x-tools/beaglebone-black/armv7-bbb-linux-gnueabihf/sysroot/usr/include

spi_test:
	${CROSS_COMPILE}gcc ${CFLAGS} -o spi_test spi_test.c
	chmod +x spi_test

clean:
	rm spi_test
