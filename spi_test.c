/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007 MontaVista Software, Inc.
 * Copyright (c) 2007 Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void pabort(const char *s)
{
	perror(s);
    abort();
}

static const char	*device = "/dev/spidev1.0";  	// device
static uint32_t 	mode;  							// mode
static uint8_t 		bits = 32; 						// number of bits
static uint32_t 	speed = 100000;  				// speed in Hz
static uint16_t 	delay;  						// delay

static void 		transfer(int fd)  				// transfer function
{
    int ret;

    uint32_t tx_command[] = {
         0x01,
		 0x01,
		 0x07,
		 0x00,
    };
	uint32_t rx_answer[1] = {0xFFFFFFFF, };

	uint32_t tx_2[128];
	memset(&tx_2, 0x00, 128 * sizeof(tx_2[0]));

	uint32_t rx_answer_2[128];
	memset(&rx_answer_2, 0xEE, 128 * sizeof(rx_answer_2[0]));

    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long) tx_command,
        .rx_buf = (unsigned long) NULL,
        .delay_usecs = delay,
        .speed_hz = speed,
        .len = ARRAY_SIZE(tx_command) * sizeof(tx_command[0]),
        .cs_change = 0
    };

	// for (int i = 0; i < ARRAY_SIZE(tx_command); i++)
    // ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);

	tr.tx_buf = (unsigned long) tx_2;
	tr.rx_buf = (unsigned long) rx_answer;
	tr.len = 4;

    // ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	// if (rx_answer[0] == 0x00) {
	// 	printf("OK!\n");
	// }

	// const buffers
	uint32_t tx_2_const[1] = {120};
	uint32_t rx_2_const[1] = {0x00};

	tr.len = 4;
	tr.tx_buf = (unsigned long) tx_2_const;
	tr.rx_buf = (unsigned long) NULL;

	uint8_t LENGTH = 1;
	for (int i = 0; i < LENGTH; i++) {
		// tr.rx_buf = (unsigned long) rx_answer_2[i];
		ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
		tr.rx_buf = (unsigned long) rx_2_const;

		ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);

		printf("%d\n", rx_2_const[0]);
	}

	puts("");
}

int main(int argc, char *argv[])
{
    int ret = 0;
    int fd;

	fd = open(device, O_RDWR);

    mode |= SPI_MODE_0;
    ret = ioctl(fd, SPI_IOC_WR_MODE32, &mode);
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);

	transfer(fd);
    close(fd);

	return ret;
}
